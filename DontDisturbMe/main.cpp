#include "main.h"
#include <DXUT.h>
#include <gtasa.hpp>
#include <samp.hpp>

AsiPlugin::AsiPlugin() : SRDescent() {
	// Constructor
	dialog = SAMP::Dialog::InstanceRef();
	input  = SAMP::Input::InstanceRef();
	font   = g_class.DirectX->d3d9_CreateFont( "Arial", 12, eFt_bold );

	if ( SAMP::isR3() ) {
		showDialog.changeAddr( 0x6FF14 );
		hideDialog.changeAddr( 0x6FF40 );
		sendCommand.changeAddr( 0x69190 );
		pickedUpPickUp.changeAddr( 0x1314D );
	}

	showDialog.install();
	showDialog.onBefore += [this] {
		if ( !dialog->isRemote() ) return;
		if ( dialog->type() == 3 ) return;

		if ( GetTickCount() - showTimer > 1000 ) {
			showDialog.skipOriginal();
			dialog->setActive( false );
			blinkTimer = GetTickCount();
		}
		isDefined = true;
	};

	hideDialog.install();
	hideDialog.onBefore += [this] {
		UpdateShowTimer();
		isDefined = false;
	};

	sendCommand.install();
	sendCommand.onBefore += std::tuple{ this, &AsiPlugin::UpdateShowTimer };

	pickedUpPickUp.install();
	pickedUpPickUp.onBefore += std::tuple{ this, &AsiPlugin::UpdateShowTimer };

	gkId = g_class.events->onGameKeyStateChanged += [this]( SRKeys::GameKeys key, uint16_t ) {
		if ( key.number == 10 || key.number == 11 || key.number == 21 ) UpdateShowTimer();
	};

	mId = g_class.events->onMainLoop += [this] {
		if ( g_vars.gameSatate != 9 ) return;
		if ( g_vars.isMenuOpened ) return;

		for ( int i = 0; i < 32; ++i ) {
			auto &marker3D	 = C3dMarker::pool()[i];
			auto &markerRace = CCheckpoint::pool()[i];

			if ( !marker3D.matrix.pos.isClear() && marker3D.m_bIsUsed ) {
				auto dist = LOCAL_PLAYER->getPosition() - marker3D.matrix.pos;
				if ( dist.length() <= marker3D.m_fSize ) UpdateShowTimer();
			}
			if ( !markerRace.m_pos.isClear() && markerRace.m_bIsUsed ) {
				auto dist = LOCAL_PLAYER->getPosition() - markerRace.m_pos;
				if ( dist.length() <= markerRace.m_fSize ) UpdateShowTimer();
			}
		}
	};

	kId = g_class.events->onKeyPressed += [this]( int key ) {
		if ( !isDefined ) return;
		if ( input->isInputEnabled() ) return;

		if ( key == VK_HOME ) {
			dialog->setActive( !dialog->isActive() );
			dialog->dialog()->m_bVisible = dialog->isActive();
			if ( !dialog->isActive() ) g_class.cursor->hideCursor();
			g_class.events->hookEvent();
		}
	};

	drwId = g_class.DirectX->onDraw += [this] {
		if ( !isDefined ) return;
		if ( dialog->isActive() ) return;
		if ( g_vars.isMenuOpened ) return;

		int lowest = input->edit()->controlData.pos[1];
		for ( int i = 0; i < input->dialog()->m_Controls.m_nSize; ++i ) {
			auto ctrl = input->dialog()->m_Controls.m_pData[i];
			if ( !ctrl->GetVisible() && !ctrl->GetEnabled() ) continue;
			auto bottom = ctrl->controlData.pos[1] + ctrl->controlData.size[1];
			if ( ctrl->controlData.GetID() == 0 ) bottom += 50;
			if ( bottom > lowest ) lowest = bottom;
		}

		SRString text( dialog->title() );
		if ( text.empty() ) {
			text = "Dialog %d:%d";
			text.args( dialog->id(), dialog->type() );
		}
		if ( GetTickCount() - blinkTimer < 1000 ) {
			if ( GetTickCount() % 2 ) font->PrintShadow( 10, lowest, -1, text );
		} else
			font->PrintShadow( 10, lowest, -1, text );
	};
}

AsiPlugin::~AsiPlugin() {
	// Destructor
	g_class.DirectX->onDraw -= drwId;
	g_class.events->onKeyPressed -= kId;
	g_class.events->onMainLoop -= mId;
	g_class.events->onGameKeyStateChanged -= gkId;
	if ( dialog ) SAMP::Dialog::DeleteInstance();
	if ( input ) SAMP::Input::DeleteInstance();
	g_class.DirectX->d3d9_ReleaseFont( font );
}

void AsiPlugin::UpdateShowTimer() {
	showTimer = GetTickCount();
}
