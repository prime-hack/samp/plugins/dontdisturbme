#ifndef MAIN_H
#define MAIN_H

#include "loader/loader.h"
#include <SRDescent.h>
#include <SRHook.hpp>
#include <d3d9/d3drender.h>
#include <samp_pimpl.hpp>

class AsiPlugin : public SRDescent {
	SRHook::Hook<> showDialog{ 0x6C014, 7, "samp" };
	SRHook::Hook<> hideDialog{ 0x6C040, 6, "samp" };
	SRHook::Hook<> sendCommand{ 0x65C60, 6, "samp" };
	SRHook::Hook<> pickedUpPickUp{ 0xFFED, 6, "samp" };

	SAMP::Dialog *dialog = nullptr;
	SAMP::Input * input	 = nullptr;
	CD3DFont *	  font	 = nullptr;

	DWORD showTimer	 = 0;
	DWORD blinkTimer = 0;

	int gkId  = -1;
	int mId	  = -1;
	int kId	  = -1;
	int drwId = -1;

	bool isDefined = false;

public:
	explicit AsiPlugin();
	virtual ~AsiPlugin();

protected:
	void UpdateShowTimer();
};

#endif // MAIN_H
